import React, { Component } from 'react';

class ListItem extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  render(props) {
    return(
      <li key={this.props.id} className="list-group-item">
        <div>{this.props.text}</div>
      </li>
    )
  }
}

export default ListItem;
